﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB04
{
    interface IRoslinowate
    {
        void Rosnij();
        void Zakwitnij();
        void Owocuj();
    }
}
