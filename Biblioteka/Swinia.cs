﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
   public class Swinia : Zwierze, IMiesozerne, IRoslinozerne
    {
        public void zjedzMieso()
        {
            Console.WriteLine($"{Nazwa} zajad miesa");
        }

        public void zjedzPozywienie()
        {
            Console.WriteLine($"{Nazwa} zajada roslinne pozywienia");
        }

         void IRoslinozerne.znajdzPozywienie()
        {
            Console.WriteLine($"{Nazwa} szuka Roslinnego pozywienia");
        }
        void IMiesozerne.znajdzPozywienie()
        {
            Console.WriteLine($"{Nazwa} szuka miesnego pozywienia");
        }
    }
}
