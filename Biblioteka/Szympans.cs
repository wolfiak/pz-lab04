﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
   public class Szympans : Zwierze, IRoslinozerne, IMiesozerne
    {
        public void zjedzMieso()
        {
            Console.WriteLine($"{Nazwa} je mieso");
        }

         public void zjedzPozywienie()
        {
            Console.WriteLine($"{Nazwa} je rosline");
        }

        void IMiesozerne.znajdzPozywienie()
        {
            Console.WriteLine($"{Nazwa} szuka miesnego pozywienia");
        }

        void IRoslinozerne.znajdzPozywienie()
        {
            Console.WriteLine($"{Nazwa} szuka miesnego pozywienia");
        }
    }
}
