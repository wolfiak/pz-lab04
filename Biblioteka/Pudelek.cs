﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Pudelek : Zwierze, IMiesozerne
    {

        public void zjedzMieso()
        {
            Console.WriteLine($"{Nazwa} je mieso");
        }

        public void znajdzPozywienie()
        {
            Console.WriteLine($"{Nazwa} szuka pozywienia");
        }
    }
}
