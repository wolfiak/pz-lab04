﻿using PZ_LAB04;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
   public class Zwierze : Istota, IRosnace
    {
        private int szybkosc;
        public int Szybkosc {
            get
            {
                return szybkosc;
            }
            set
            {
                if(value > 1225)
                {
                    throw new Exception("Nie moze byc szybszy od dzwieku");
                }else
                {
                    szybkosc = value;
                }
            }
        }

        public void Rosnij()
        {
            wysokosc *= (5 / 100);
        }
    }
}
