﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public abstract class Istota
    {
        public string Nazwa { get; set; }
        public int wysokosc { get; set; }
        public string Lacinska { get; set; }
    }
}
