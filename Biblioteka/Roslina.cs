﻿using PZ_LAB04;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    class Roslina :Istota, IRosnace,IRoslinowate
    {
        public int iloscWytwarzangeoTlenu { get; set; }
        public Stan stan { get; set; }

        public void Owocuj()
        {
            stan = Stan.Zaowocowala;
        }

        public void Rosnij()
        {
            wysokosc += wysokosc * iloscWytwarzangeoTlenu/10;
            stan = Stan.Rosnie;
        }

        public void Zakwitnij()
        {
            stan = Stan.Kwitnie;
        }
    }
    enum Stan
    {
        Rosnie,
        Kwitnie,
        Zakwitla,
        Zaowocowala
    }
}
