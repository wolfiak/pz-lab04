﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Duch : Istota
    {
        private int przezroczystosc;
        public int Przezroczystosc {
            get
            {
                return przezroczystosc;
            }
            set
            {
                if(value < 0 || value > 100)
                {
                    throw new Exception("Nie moze byc mniej niz zero albo wiecej niz 100");
                }else
                {
                    przezroczystosc = value;
                }
            }
        }
        public int Ektoplazma { get; set; }
        public string metodaDoStraszenia { get; set; }

    }
}
