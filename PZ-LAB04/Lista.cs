﻿using Biblioteka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB04
{
    static class Lista
    {
        public static List<Istota> lista { get; set; } = new List<Istota>();
        public static List<IRoslinozerne> listaR { get; set; } = new List<IRoslinozerne>();
        public static List<IMiesozerne> listaM { get; set; } = new List<IMiesozerne>();

        public static void Dodawanie()
        {
            lista.Add(new Swinia
            {
                Nazwa="pepe",
               Lacinska="swinius erectus",
              Szybkosc=20,
              wysokosc=2  
            });
            lista.Add(new Duch
            {
                Ektoplazma=20,
                wysokosc=2,
                Lacinska="duchus erec",
                metodaDoStraszenia="bicie",
               Nazwa="kacper",
               Przezroczystosc=20 

            });
            lista.Add(new Szympans
            {
                Lacinska="szympanus",
                Nazwa="tytus",
                Szybkosc=20,
                wysokosc=2
            });
        }
        public static void transformuj()
        {
            lista.ForEach(k =>
            {
            if (k is IMiesozerne)
            {
                listaM.Add((IMiesozerne)k);
            }
                if (k is IRoslinozerne)
            {
                listaR.Add((IRoslinozerne) k);
                }
               
            });
        }
        public static void karmMiesem()
        {
            listaM.ForEach(i =>
            {
                i.zjedzMieso();
            });
        }
        public static void karmRoslina()
        {
            listaR.ForEach(i =>
            {
              
                i.znajdzPozywienie();
                i.zjedzPozywienie();
            });
        }
        public static void karmMiesozerne()
        {
            listaM.ForEach(i => {
                i.znajdzPozywienie();
                i.zjedzMieso();
            });
        }
        public static void karmToiTO()
        {
            karmRoslina();
            karmMiesozerne();
        }
        public static void karmMJednego(IMiesozerne m)
        {
            m.znajdzPozywienie();
            m.zjedzMieso();
        }
        public static void karmRJdnego(IRoslinozerne r)
        {
            r.znajdzPozywienie();
            r.zjedzPozywienie();
        }
        public static void wyswietl()
        {
            lista.ForEach(i =>
            {

            });
        }

    }
}
